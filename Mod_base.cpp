#include <cstdint>
#include <cassert>
#include <cerrno>
#include <climits>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <stdexcept>
#include <string>
using std::cout;
using std::endl;
using std::string;
using std::to_string;

class Mod
{
	private:

		int64_t a;

		int64_t n;


	public:
		Mod(int64_t a,int64_t n)
		{
			if ( n <= 0 )
			{
				errno = -1;
				
				perror("Modulus base number n must not be less than or equal to 0");
				
				exit(-1);

			}
			
			this->n = n;

			this->a = modulus(a,n);


		}

		int64_t modulus(int64_t a,int64_t n)
		{
			int64_t m = a % n;

			return ( m < 0 ) ? m + n : m;
		}


		string toString(void)
		{
			return to_string(this->a) + " mod " + to_string(this->n);
		}

};

int main(void)
{

	Mod test = Mod(-23,5);

	return 0;
}
