#include <cstdint>
#include <cassert>
#include <cerrno>
#include <climits>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <stdexcept>
#include <string>
using std::cout;
using std::endl;
using std::string;
using std::to_string;

class Mod
{
	private:

		int64_t a;

		int64_t n;


	public:
		Mod(int64_t a,int64_t n)
		{
			if ( n <= 0 )
			{
				errno = -1;
				
				perror("Modulus base number n must not be less than or equal to 0");
				
				exit(-1);

			}
			
			this->n = n;

			this->a = modulus(a,n);


		}

		int64_t modulus(int64_t a,int64_t n)
		{
			int64_t m = a % n;

			return ( m < 0 ) ? m + n : m;
		}
		
		int64_t geta(void)
		{
			return this->a;
		}

		int64_t getn(void)
		{
			return this->n;
		}

		// Lowest possible value for x in operator+(int64_t b)
		
		// is -(2^63 - 2) = -2^63 + 2 although LLONG_MIN = -2^63
		//
		// since the Mod(int64_t a,int64_t n) constructor applies the input "a"
		//
		// to modulus(int64_t a).
		
		int64_t absolute_value(int64_t x)
		{
			return x < 0 ? -x : x;
		}
	
		// both a and b in (a + b) \mod n are positive

		Mod operator+(int64_t b)
		{
			int64_t a_neg = this->a - n; 

			int64_t b_pos = modulus(b,this->n);
			
			int64_t b_neg = b_pos - n;

			int64_t a_add = this->a < absolute_value(a_neg) ? this->a : a_neg;
			
			int64_t b_add = b_pos < absolute_value(b_neg) ? b_pos : b_neg;

			return Mod( a_add + b_add , this->n ); 	
		}
		
		Mod operator+(Mod b)
		{
			assert(b.getn() == this->n);

			int64_t a_neg = this->a - n; 

			int64_t b_pos = modulus(b.geta(),this->n);
			
			int64_t b_neg = b_pos - n;

			int64_t a_add = this->a < absolute_value(a_neg) ? this->a : a_neg;
			
			int64_t b_add = b_pos < absolute_value(b_neg) ? b_pos : b_neg;

			return Mod( a_add + b_add , this->n ); 	
		}
		
		Mod operator-(int64_t b)
		{
			int64_t a_neg = this->a - n; 

			int64_t b_pos = modulus(b,this->n);
			
			int64_t b_neg = b_pos - n;

			int64_t a_add = this->a < absolute_value(a_neg) ? this->a : a_neg;
			
			int64_t b_add = b_pos < absolute_value(b_neg) ? b_pos : b_neg;

			return Mod( a_add - b_add, this->n ); 	
		}
		
		Mod operator*(Mod b)
		{
			int64_t a_neg = this->a - n; 

			int64_t b_pos = modulus(b.geta(),this->n);
			
			int64_t b_neg = b_pos - n;

			int64_t a_mul = this->a < absolute_value(a_neg) ? this->a : a_neg;
			
			int64_t b_mul = b_pos < absolute_value(b_neg) ? b_pos : b_neg;
		
			int64_t throwaway = 0;
			
			if ( __builtin_mul_overflow(a_mul,b_mul,(int64_t*) &throwaway) == false )
			{
				return Mod(a_mul * b_mul,n);
			}
			
			return

				Mod(a_mul * (LLONG_MAX / a_mul),n) * ( b_mul / ( LLONG_MAX / a_mul ) )

				+

				Mod(a_mul,n) * ( b_mul - ( LLONG_MAX / a_mul ) * ( b_mul / ( LLONG_MAX / a_mul ) ) )

				;
 


		}
		
		Mod operator*(int64_t b)
		{
			int64_t a_neg = this->a - n; 

			int64_t b_pos = modulus(b,this->n);
			
			int64_t b_neg = b_pos - n;

			int64_t a_mul = this->a < absolute_value(a_neg) ? this->a : a_neg;
			
			int64_t b_mul = b_pos < absolute_value(b_neg) ? b_pos : b_neg;
		
			int64_t throwaway = 0;
			
			if ( __builtin_mul_overflow(a_mul,b_mul,(int64_t*) &throwaway) == false )
			{
				return Mod(a_mul * b_mul,n);
			}
			
			return

				Mod(a_mul * (LLONG_MAX / a_mul),n) * ( b_mul / ( LLONG_MAX / a_mul ) )

				+

				Mod(a_mul,n) * ( b_mul - ( LLONG_MAX / a_mul ) * ( b_mul / ( LLONG_MAX / a_mul ) ) )

				;
 

		}

		string toString(void)
		{
			return to_string(this->a) + " mod " + to_string(this->n);
		}

};

int main(void)
{
	uint64_t a = 0;

	Mod test = Mod(7,31);

	Mod sol = test * 7;

	assert(sol.geta() == 18);

	sol = test * 0;

	assert(sol.geta() == 0);
	
	test = Mod(6,31);

	sol = test * 9;

	assert(sol.geta() == 23);
	
	test = Mod(15,31);

	sol = test * 15;

	assert(sol.geta() == 8);
	
	test = Mod(50,101);

	sol = test * 50;

	assert(sol.geta() == 76);
	
	test = Mod(10,101);

	sol = test * 11;

	assert(sol.geta() == 9);
	
	test = Mod(25,101);

	sol = test * 25;

	assert(sol.geta() == 19);
	
	test = Mod(11,31);

	sol = test * 9;

	assert(sol.geta() == 6);
	
	test = Mod(7,31);

	sol = test * 6;

	assert(sol.geta() == 11);
	
	test = Mod(15,31);

	sol = test * 3;

	assert(sol.geta() == 14);
	
	test = Mod(8,31);

	sol = test * 8;

	assert(sol.geta() == 2);
	
	test = Mod(-7,31);

	sol = test * (-7);

	assert(sol.geta() == 18);
	
	test = Mod(-6,31);

	sol = test * (-9);

	assert(sol.geta() == 23);
	
	test = Mod(-15,31);

	sol = test * (-15);

	assert(sol.geta() == 8);
	
	test = Mod(-50,101);

	sol = test * (-50);

	assert(sol.geta() == 76);
	
	test = Mod(-15,31);

	sol = test * (-15);

	assert(sol.geta() == 8);
	
	test = Mod(4,31);

	sol = test * 15;

	assert(sol.geta() == 29);
	
	test = Mod(3,31);

	sol = test * 15;

	assert(sol.geta() == 14);
	
	test = Mod(-11,31);

	sol = test * (-9);

	assert(sol.geta() == 6);
	
	test = Mod(-7,31);

	sol = test * (-6);

	assert(sol.geta() == 11);
	
	test = Mod(-15,31);

	sol = test * (-3);

	assert(sol.geta() == 14);
	
	test = Mod(-8,31);

	sol = test * (-8);

	assert(sol.geta() == 2);
	
	test = Mod(-4,31);

	sol = test * (-15);

	assert(sol.geta() == 29);
	
	test = Mod(-5,31);

	sol = test * (-15);

	assert(sol.geta() == 13);
	
	test = Mod(-16,31);

	sol = test * (-3);

	assert(sol.geta() == 17);

	a = (uint64_t)pow(2,62);

	a--;
	
	test = Mod(a,LLONG_MAX);

	sol = test * a;

	assert(sol.geta() == 2305843009213693952);
	
	a = (uint64_t)pow(2,62);

	a--;
	
	test = Mod(a,LLONG_MAX);

	sol = test * (-a);

	assert(sol.geta() == 6917529027641081855);
	
	a = (uint64_t)pow(2,62);

	a--;
	
	test = Mod(-a,LLONG_MAX);

	sol = test * (-a);

	assert(sol.geta() == 2305843009213693952);
	
	a = (uint64_t)pow(2,32);
	
	test = Mod(a,LLONG_MAX);

	sol = test * a;

	assert(sol.geta() == 2);
	
	a = (uint64_t)pow(2,32);
	
	test = Mod(a,LLONG_MAX);

	sol = test * (-a);

	assert(sol.geta() == 9223372036854775805);
	
	a = (uint64_t)pow(2,32);
	
	test = Mod(-a,LLONG_MAX);

	sol = test * (-a);

	assert(sol.geta() == 2);
	
	a = (uint64_t)pow(2,16);
	
	test = Mod(a,LLONG_MAX);

	sol = test * (a);

	assert(sol.geta() == 4294967296);
	
	a = (uint64_t)pow(2,16);
	
	test = Mod(-a,LLONG_MAX);

	sol = test * a;

	assert(sol.geta() == 9223372032559808511);
	
	a = (uint64_t)pow(2,16);
	
	test = Mod(-a,LLONG_MAX);

	sol = test * (-a);

	assert(sol.geta() == 4294967296);
	
	a = (uint64_t)pow(2,62);

	a--;
	
	test = Mod(-a,LLONG_MAX);

	sol = test * (-a);

	assert(sol.geta() == 2305843009213693952);
	
	a = (uint64_t)pow(2,62);
	
	test = Mod(a,LLONG_MAX);

	sol = test * (-a);

	assert(sol.geta() == 6917529027641081855);
	
	a = (uint64_t)pow(2,62);

	a--;
	
	test = Mod(a,LLONG_MAX);

	sol = test * (-a);

	assert(sol.geta() == 6917529027641081855);
	
	test = Mod(LLONG_MAX,LLONG_MAX);

	sol = test * (LLONG_MAX);

	assert(sol.geta() == 0);
	
	test = Mod(-LLONG_MAX,LLONG_MAX);

	sol = test * (-LLONG_MAX);

	assert(sol.geta() == 0);
	
	test = Mod(LLONG_MAX,LLONG_MAX);

	sol = test * (-LLONG_MAX);

	assert(sol.geta() == 0);
	
	test = Mod(-LLONG_MAX,LLONG_MAX);

	sol = test * (LLONG_MAX);

	assert(sol.geta() == 0);

	return 0;
}
