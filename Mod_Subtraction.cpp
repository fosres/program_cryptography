#include <cstdint>
#include <cassert>
#include <cerrno>
#include <climits>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <stdexcept>
#include <string>
using std::cout;
using std::endl;
using std::string;
using std::to_string;

class Mod
{
	private:

		int64_t a;

		int64_t n;


	public:
		Mod(int64_t a,int64_t n)
		{
			if ( n <= 0 )
			{
				errno = -1;
				
				perror("Modulus base number n must not be less than or equal to 0");
				
				exit(-1);

			}
			
			this->n = n;

			this->a = modulus(a,n);


		}

		int64_t modulus(int64_t a,int64_t n)
		{
			int64_t m = a % n;

			return ( m < 0 ) ? m + n : m;
		}
		
		int64_t geta(void)
		{
			return this->a;
		}

		int64_t getn(void)
		{
			return this->n;
		}

		// Lowest possible value for x in operator+(int64_t b)
		
		// is -(2^63 - 2) = -2^63 + 2 although LLONG_MIN = -2^63
		//
		// since the Mod(int64_t a,int64_t n) constructor applies the input "a"
		//
		// to modulus(int64_t a).
		
		int64_t absolute_value(int64_t x)
		{
			return x < 0 ? -x : x;
		}
	
		// both a and b in (a + b) \mod n are positive

		Mod operator+(int64_t b)
		{
			int64_t a_neg = this->a - n; 

			int64_t b_pos = modulus(b,this->n);
			
			int64_t b_neg = b_pos - n;

			int64_t a_add = this->a < absolute_value(a_neg) ? this->a : a_neg;
			
			int64_t b_add = b_pos < absolute_value(b_neg) ? b_pos : b_neg;

			return Mod( a_add + b_add , this->n ); 	
		}
		
		Mod operator-(int64_t b)
		{
			int64_t a_neg = this->a - n; 

			int64_t b_pos = modulus(b,this->n);
			
			int64_t b_neg = b_pos - n;

			int64_t a_add = this->a < absolute_value(a_neg) ? this->a : a_neg;
			
			int64_t b_add = b_pos < absolute_value(b_neg) ? b_pos : b_neg;

			return Mod( a_add - b_add, this->n ); 	
		}

		string toString(void)
		{
			return to_string(this->a) + " mod " + to_string(this->n);
		}

};

int main(void)
{
	uint64_t a = (uint64_t)pow(2,63);
	
	Mod test = Mod(0,5);

	Mod sol = test - 0;
	
	assert(test.geta() == 0);

	test = Mod(4,5);

	sol = test - (-1);

	assert(sol.geta() == 0);
	
	test = Mod(6,12);
	
	sol = test - (-9);
	
	assert(sol.geta() == 3);

	test = Mod(12,12);

	sol = test - 0;
	
	assert(sol.geta() == 0);
	
	test = Mod(LLONG_MAX,LLONG_MAX);

	sol = test - (-2);

	assert(sol.geta() == 2);

	test = Mod(1024,8);

	sol = test - (-1024);	
	
	assert(sol.geta() == 0);
	
	test = Mod(LLONG_MIN,LLONG_MAX);
	
	sol = test - (-LLONG_MAX);	
	
	assert(sol.geta() == 9223372036854775806);

	test = Mod(0,LLONG_MAX);
	
	sol = test - (-1);	
	
	assert(sol.geta() == 1);
	
	test = Mod(LLONG_MAX,LLONG_MAX);

	sol = test - (-LLONG_MAX);

	assert(sol.geta() == 0);
	
	test = Mod(LLONG_MAX,1);

	sol = test - (-LLONG_MAX);

	assert(sol.geta() == 0);

	a = (uint64_t)pow(2,63);
	
	test = Mod(a - 2,LLONG_MAX);

	sol = test - (-a + 2);

	assert(sol.geta() == 9223372036854775805);

	a = (uint64_t)pow(2,62);

	test = Mod(a,LLONG_MAX);

	sol = test - (-a);
	
	assert(sol.geta() == 1);
	
	a = (uint64_t)pow(2,61);

	test = Mod(a,LLONG_MAX);

	sol = test - (-a);
	
	assert(sol.geta() == 4611686018427387904);

	test = Mod(256,16);

	sol = test - (-256);
	
	assert(sol.geta() == 0);
	
	test = Mod(3,2);

	sol = test - (-2);
	
	assert(sol.geta() == 1);
	
	test = Mod(432,27);

	sol = test - (-501);
	
	assert(sol.geta() == 15);
		
	test = Mod(LLONG_MIN,LLONG_MAX);

	sol = test - LLONG_MIN;
	
	assert(sol.geta() == 0);
	
	test = Mod(LLONG_MAX,LLONG_MAX);

	sol = test - LLONG_MIN;
	
	assert(sol.geta() == 1);
	
	test = Mod(-3,5);

	sol = test - (-2);
	
	assert(sol.geta() == 4);

	test = Mod(1,LLONG_MAX);

	sol = test - LLONG_MIN;
	
	assert(sol.geta() == 2);
	
	test = Mod(-1,LLONG_MAX);

	sol = test - LLONG_MIN;
	
	assert(sol.geta() == 0);
	
	test = Mod(LLONG_MIN,LLONG_MAX);

	sol = test - (-1);
	
	assert(sol.geta() == 0);
	
	test = Mod(-1,LLONG_MAX);

	sol = test - (-1);

	assert(sol.geta() == 0);
	
//	assert(sol.geta() == 9223372036854775805);

	a = (uint64_t)pow(2,62);
	
	test = Mod(-a,LLONG_MAX);

	sol = test - a;
	
	assert(sol.geta() == 9223372036854775806);

	a = (uint64_t)pow(2,63);
	
	test = Mod(a - 3,LLONG_MAX);

	sol = test - (-a + 3);
	
	assert(sol.geta() == 9223372036854775803);
	
	a = (uint64_t)pow(2,62);
	
	test = Mod(a + 2,LLONG_MAX);

	sol = test - (-a - 5);
	
	assert(sol.geta() == 8);
	
	a = (uint64_t)pow(2,63);
	
	test = Mod(-a + 3,LLONG_MAX);

	sol = test - (a - 3);
	
	assert(sol.geta() == 4);
	
	a = (uint64_t)pow(2,62);
	
	test = Mod(-a - 2,LLONG_MAX);

	sol = test - (a + 5);
	
	assert(sol.geta() == 9223372036854775799);
	
	a = (uint64_t)pow(2,63);
	
	test = Mod(-a + 2,LLONG_MAX);

	sol = test - (a - 2);
	
	assert(sol.geta() == 2);
	
	test = Mod(-3,2);

	sol = test - (5);
	
	assert(sol.geta() == 0);
	
	test = Mod(3,2);

	sol = test - (5);
	
	assert(sol.geta() == 0);
	
	a = (uint64_t)pow(2,63);
	
	test = Mod(a - 2,LLONG_MAX);

	sol = test - (a - 2);
	
	assert(sol.geta() == 0);
	
	test = Mod(LLONG_MAX,LLONG_MAX);

	sol = test - (LLONG_MAX);
	
	assert(sol.geta() == 0);
	
	test = Mod(LLONG_MIN,LLONG_MAX);

	sol = test - (LLONG_MAX);
	
	assert(sol.geta() == 9223372036854775806);
	
	test = Mod(LLONG_MIN,LLONG_MAX);

	sol = test - (LLONG_MAX);
	
	assert(sol.geta() == 9223372036854775806);
	
	test = Mod(LLONG_MAX,LLONG_MAX);

	sol = test - (1);
	
	assert(sol.geta() == 9223372036854775806);
	
	test = Mod(100,3);

	sol = test - 15;
	
	assert(sol.geta() == 1);
	
	test = Mod(-100,3);

	sol = test - (-15);
	
	assert(sol.geta() == 2);
	
	test = Mod(-48,100);

	sol = test - 16;
	
	assert(sol.geta() == 36);
	
	test = Mod(4,1);

	sol = test - 5;
	
	assert(sol.geta() == 0);

	return 0;
}
