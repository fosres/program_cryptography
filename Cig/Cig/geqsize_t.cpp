#include <iostream>
#include <cstdint>
#include <cassert>
using std::cout;
using std::endl;
		/*
		leqsize_t() accepts size_t integers a and b and tests if a <= b.

		If True returns ~0 as as a size_t integer

		If False returns 0 as a size_t integer

		Algorithm Taken from Hacker's Delight Second Edition: 2-12: Comparision Predicates
		*/

		size_t leqsize_t(size_t a,size_t b)
		{
			size_t cmp = ( ( ~a | b ) & ( ( a ^ b ) | ~(b - a) ) ) >> ( sizeof(size_t) * 8 - 1);


			return ~cmp + 1;
		}

		/*
		geqsize_t() accepts size_t integers a and b and tests if a >= b.

		If True returns ~0 as as a size_t integer

		If False returns 0 as a size_t integer

		Algorithm Taken from Hacker's Delight Second Edition: 2-12: Comparision Predicates
		*/

		size_t geqsize_t(size_t a,size_t b)
		{
			return leqsize_t(b,a); 
		}
int main(void)
{

	assert(geqsize_t(~0,1) == ~0);

	assert(geqsize_t(1,~0) == 0);
	
	assert(geqsize_t(0xf,0xe) == ~0);
	
	assert(geqsize_t(0xe,0xf) == 0);
	
	assert(geqsize_t(3,4) == 0);
	
	assert(geqsize_t(4,3) == ~0);
	
	assert(geqsize_t(0xffff,1) == ~0);
	
	assert(geqsize_t(1,0xffff) == 0);
	
	assert(geqsize_t(0xfffffffe,0xffffffff) == 0);
	
	assert(geqsize_t(0xffffffff,0xfffffffe) == ~0);
	
	assert(geqsize_t(0xffffffff,0xffffffff) == ~0);

	return 0;
}

