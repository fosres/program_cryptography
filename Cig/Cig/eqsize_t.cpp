#include <cassert>
#include <iostream>
#include <cstdint>

		/*
		neqsize_t() accepts size_t integers a and b and tests if a != b.

		If True returns ~0 as as a size_t integer

		If False returns 0 as a size_t integer

		Algorithm Taken from Hacker's Delight Second Edition: 2-12: Comparision Predicates
		*/

		size_t neqsize_t(size_t a,size_t b)
		{
			size_t cmp = ( a - b | b - a ) >> ( sizeof(size_t) * 8 - 1 );

			return ~cmp + 1;
		}
		
		/*
		eqsize_t() accepts size_t integers a and b and tests if a == b.

		If True returns ~0 as as a size_t integer

		If False returns 0 as a size_t integer

		Algorithm Taken from Hacker's Delight Second Edition: 2-12: Comparision Predicates
		*/

		size_t eqsize_t(size_t a,size_t b)
		{
			return ~neqsize_t(a,b);
		}

int main(void)
{
	assert(eqsize_t(~0,0) == 0);

	assert(eqsize_t(~0,~0) == ~0);

	assert(eqsize_t(3,4) == 0);

	assert(eqsize_t(4,3) == 0);

	assert(eqsize_t(0xff,0xff) == ~0);

	assert(eqsize_t(0xffff,0xfffe) == 0);
	
	assert(eqsize_t(35,35) == ~0);

	return 0;
}
