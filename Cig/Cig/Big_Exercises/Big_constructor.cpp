#include <cstdint>
#include <iostream>
#include <string>
#include <vector>
using std::cout;
using std::endl;
using std::string;
using std::to_string;
using std::vector;

class Big
{
	private:
		
		vector<uint8_t> number;

		size_t bitsize = 0;

		uint64_t errcode = 0;
	
	public:

		Big (string num)
		{
		
		}

		~Big()
		{
		
		}

/*
		ltuint64_t() accepts uint64_t integers a and b and tests if a < b.

		If True returns ~0 as as a uint64_t integer

		If False returns 0 as a uint64_t integer

		Algorithm Taken from Hacker's Delight Second Edition: 2-12: Comparision Predicates
*/

		uint64_t ltuint64_t(uint64_t a,uint64_t b)
		{
			uint64_t cmp = ( ( a >> 1 ) - ( b >> 1 ) - (~a & b & 1) ) >> ( sizeof(uint64_t) * 8 - 1 );

			/*

			uint64_t result = 0;

			while ( counter )
			{
				result = ~result;
				
				counter--;
			}

			*/

			// return result;
			
			return ~cmp + 1;
		}
		
		/*
		lequint64_t() accepts uint64_t integers a and b and tests if a <= b.

		If True returns ~0 as as a uint64_t integer

		If False returns 0 as a uint64_t integer

		Algorithm Taken from Hacker's Delight Second Edition: 2-12: Comparision Predicates
		*/

		uint64_t lequint64_t(uint64_t a,uint64_t b)
		{
			uint64_t cmp = ( ( ~a | b ) & ( ( a ^ b ) | ~(b - a) ) ) >> ( sizeof(uint64_t) * 8 - 1);

			return ~cmp + 1;
		}

		/*
		 * gtuint64_t.cpp compares uint64_t integers a and b
		 *
		 * and tests if a > b
		 *
		 * Returns ~0 as a uint64_t integer if a > b
		 *
		 * Returns 0 as a uint64_t integer if a <= b
		 */

		uint64_t gtuint64_t(uint64_t a,uint64_t b)
		{
			return ~lequint64_t(a,b);
		}
		
		/*
		gequint64_t() accepts uint64_t integers a and b and tests if a >= b.

		If True returns ~0 as as a uint64_t integer

		If False returns 0 as a uint64_t integer

		Algorithm Taken from Hacker's Delight Second Edition: 2-12: Comparision Predicates
		*/

		uint64_t gequint64_t(uint64_t a,uint64_t b)
		{
			return lequint64_t(b,a); 
		}

		/*
		nequint64_t() accepts uint64_t integers a and b and tests if a != b.

		If True returns ~0 as as a uint64_t integer

		If False returns 0 as a uint64_t integer

		Algorithm Taken from Hacker's Delight Second Edition: 2-12: Comparision Predicates
		*/

		uint64_t nequint64_t(uint64_t a,uint64_t b)
		{
			uint64_t cmp = ( a - b | b - a ) >> ( sizeof(uint64_t) * 8 - 1 );

			return ~cmp + 1;
		}
		
		/*
		equint64_t() accepts uint64_t integers a and b and tests if a == b.

		If True returns ~0 as as a uint64_t integer

		If False returns 0 as a uint64_t integer

		Algorithm Taken from Hacker's Delight Second Edition: 2-12: Comparision Predicates
		*/

		uint64_t equint64_t(uint64_t a,uint64_t b)
		{
			return ~nequint64_t(a,b);
		}
};

int main(void)
{
	return 0;
}
