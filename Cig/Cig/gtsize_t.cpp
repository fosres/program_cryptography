#include <cassert>
#include <iostream>
#include <cstdint>

		/*
		leqsize_t() accepts size_t integers a and b and tests if a <= b.

		If True returns ~0 as as a size_t integer

		If False returns 0 as a size_t integer

		Algorithm Taken from Hacker's Delight Second Edition: 2-12: Comparision Predicates
		*/

		size_t leqsize_t(size_t a,size_t b)
		{
			size_t cmp = ( ( ~a | b ) & ( ( a ^ b ) | ~(b - a) ) ) >> ( sizeof(size_t) * 8 - 1);


			return ~cmp + 1;
		}


		/*
		 * gtsize_t.cpp compares size_t integers a and b
		 *
		 * and tests if a > b
		 *
		 * Returns ~0 as a size_t integer if a > b
		 *
		 * Returns 0 as a size_t integer if a <= b
		 */

		size_t gtsize_t(size_t a,size_t b)
		{
			return ~leqsize_t(a,b);
		}

int main(void)
{
	assert(gtsize_t(~0,1) == ~0);

	assert(gtsize_t(1,~0) == 0);

	assert(gtsize_t(3,4) == 0);

	assert(gtsize_t(4,3) == ~0);

	assert(gtsize_t(0xf,0xe) == ~0);

	assert(gtsize_t(0xf,0xf) == 0);

	return 0;
}
