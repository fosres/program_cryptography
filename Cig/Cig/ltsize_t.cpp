#include <cstdint>
#include <cassert>
#include <iostream>
#include <string>
using std::string;
using std::cout;
using std::endl;

		/*
		ltsize_t() accepts size_t integers a and b and tests if a < b.

		If True returns ~0 as as a size_t integer

		If False returns 0 as a size_t integer

		Algorithm Taken from Hacker's Delight Second Edition: 2-12: Comparision Predicates
		*/

		size_t ltsize_t(size_t a,size_t b)
		{
			size_t cmp =  ( ( a >> 1 ) - ( b >> 1 ) - (~a & b & 1) ) >> ( sizeof(size_t) * 8 - 1 );

			return ~cmp + 1;
		}
int main(void)
{
	size_t cmp = 0;

	assert(ltsize_t(0,1) == ~cmp);
	
	assert(ltsize_t(1,0) == cmp);
	
	assert(ltsize_t(0,~cmp) == ~cmp);
	
	assert(ltsize_t(~cmp,0) == cmp);
	
	assert(ltsize_t(~0,~0 - 1) == cmp);
	
	assert(ltsize_t(~0 - 1,~0) == ~cmp);
	
	assert(ltsize_t(3,4) == ~cmp);
	
	assert(ltsize_t(50,52) == ~cmp);
	
	assert(ltsize_t(52,50) == cmp);
	
	assert(ltsize_t(15,20) == ~cmp);
	
	assert(ltsize_t(20,15) == cmp);
	
	assert(ltsize_t(0x0000ffff,0xffff0000) == ~cmp);
	
	assert(ltsize_t(0xf,0xf) == cmp);

	return 0;
}

