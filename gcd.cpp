#include <cassert>
#include <climits>
#include <cstdint>
#include <iostream>
using std::cout;
using std::endl;

// Algorithm 2.104 Page 66 from Handbook of Applied Cryptography

// It is Integer Safe to use the '%' operator since it gives
//
// the correct result when a >= 0 and b > 0

// It accepts as input two positive integers a and b where a >= b

// In the below implementation where a < b the values of a and b are switched.

int64_t gcd(int64_t a,int64_t b)
{
	assert(a > 0);
	
	assert(b > 0);

	int64_t temp = 0;

	if ( a < b )
	{
		temp = a;

		a = b;

		b = temp;

	}

	int64_t r = 0;

	while ( b != 0 )
	{
		r = a % b;

		a = b;

		b = r;	
	}

	return a;
}

int main(void)
{

	assert(gcd(LLONG_MAX,LLONG_MAX) == LLONG_MAX);
	
	assert(gcd(LLONG_MAX,1) == 1);

	assert(gcd(4864,3458) == 38);
	
	assert(gcd(1071,462) == 21);
	
	assert(gcd(270,192) == 6);
	
	assert(gcd(1512,444) == 12);
	
	assert(gcd(35,10) == 5);
	
	assert(gcd(112,42) == 14);
	
	assert(gcd(15,10) == 5);
	
	assert(gcd(35,10) == 5);
	
	assert(gcd(31,2) == 1);
	
	assert(gcd(30,20) == 10);
	
	assert(gcd(35,15) == 5);
	
	assert(gcd(33,27) == 3);
	
	assert(gcd(93,42) == 3);
	
	assert(gcd(36,48) == 12);
	
	assert(gcd(724,21) == 1);
	
	assert(gcd(97,60) == 1);
	
	assert(gcd(26,5) == 1);
	
	assert(gcd(12,30) == 6);
	
	assert(gcd(123,36) == 3);
	
	assert(gcd(198,168) == 6);
	
	assert(gcd(3915,825) == 15);
	
	assert(gcd(34,19) == 1);
	
	assert(gcd(177741,149553) == 783);
	
	assert(gcd(816,2260) == 4);

	return 0;
}
