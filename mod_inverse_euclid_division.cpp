#include <cassert>
#include <climits>
#include <cmath>
#include <cstdint>
#include <iostream>
#include <numeric>
#include <vector>
using std::cout;
using std::endl;
using std::gcd;
using std::vector;

int64_t modulus(int64_t a,int64_t n)
{
	if ( n <= 0 ) return -1;

	int64_t m = a % n;

	return ( m < 0 ) ? m + n : m;
}

// 0 < a < m and m is prime
//
// Algorithm obtained from: https://cp-algorithms.com/algebra/module-inverse.html

int64_t mod_inverse(int64_t a, int64_t m)
{

	if ( m < 2 ) { errno = -1; perror("m in mod_inverse() is not a prime number as expected"); exit(errno); }

	if ( a < 0 ) { errno = -2; perror("a is in mod_inverse() must be positive"); exit(errno); }

	if ( a >= m ) { errno = -3; perror("a >= m in mod_inverse(). 0 < a < m."); exit(errno); }

	if ( gcd(a,m) != 1 ) return -1;
	
	return a <= 1 ? a : m - (m/a) * mod_inverse(m % a,m) % m;
}

int main(void)
{
	assert(mod_inverse(2,5) == 3);
	
	assert(mod_inverse(4,5) == 4);
	
	assert(mod_inverse(171,283) == 48);
	
	assert(mod_inverse(6,11) == 2);
	
	assert(mod_inverse(100,1259) == 768);
	
	assert(mod_inverse(899,7919) == 4466);
	
	assert(mod_inverse(486,487) == 486);
	
	assert(mod_inverse(7918,7919) == 7918);
	
	assert(mod_inverse(999,5011) == 2192);
	
	assert(mod_inverse(1023,6197) == 315);
	
	assert(mod_inverse(3999,4447) == 943);
	
	assert(mod_inverse(3999,4447) == 943);
	
	assert(mod_inverse(2000,3469) == 2378);

	uint64_t a = (uint64_t)pow(2,60), m = (uint64_t)pow(2,61) - 1;

	assert(mod_inverse((uint64_t)pow(2,60),m) == 2);

	return 0;
}
