#include <cassert>
#include <climits>
#include <cstdint>
#include <iostream>
using std::cout;
using std::endl;


// Algorithm 2.104 Page 66 from Handbook of Applied Cryptography

// It is Integer Safe to use the '%' operator since it gives
//
// the correct result when a >= 0 and b > 0

// It accepts as input two positive integers a and b where a >= b

// In the below implementation where a < b the values of a and b are switched.

uint64_t binary_gcd(uint64_t x,uint64_t y)
{
	if ( x == 0 ) return 0;

	assert( y > 0 );

	uint64_t temp = 0;

	if ( x < y )
	{
		temp = x;

		x = y;

		y = temp;

	}
	
	uint64_t g = 1;

	while ( x % 2 == 0 && y % 2 == 0 )
	{
		x >>= 1; y >>= 1; g <<= 1;	
	}

	while ( x != 0 )
	{

		while ( x % 2 == 0 ) x >>= 1;
		
		while ( y % 2 == 0 ) y >>= 1;
	
		uint64_t t = ( x >= y ) ? (x - y) / 2 : (y - x) / 2;
		
		if ( x >= y ) x = t;

		else y = t;	
	}
	
	return g * y;
}

int main(void)
{
	assert(binary_gcd(0,1) == 0);

	assert(binary_gcd(ULLONG_MAX,ULLONG_MAX) == ULLONG_MAX);
	
	assert(binary_gcd(ULLONG_MAX,1) == 1);

	assert(binary_gcd(4864,3458) == 38);
	
	assert(binary_gcd(1071,462) == 21);
	
	assert(binary_gcd(270,192) == 6);
	
	assert(binary_gcd(1512,444) == 12);
	
	assert(binary_gcd(35,10) == 5);
	
	assert(binary_gcd(112,42) == 14);
	
	assert(binary_gcd(15,10) == 5);
	
	assert(binary_gcd(35,10) == 5);
	
	assert(binary_gcd(31,2) == 1);
	
	assert(binary_gcd(30,20) == 10);
	
	assert(binary_gcd(35,15) == 5);
	
	assert(binary_gcd(33,27) == 3);
	
	assert(binary_gcd(93,42) == 3);
	
	assert(binary_gcd(36,48) == 12);
	
	assert(binary_gcd(724,21) == 1);
	
	assert(binary_gcd(97,60) == 1);
	
	assert(binary_gcd(26,5) == 1);
	
	assert(binary_gcd(12,30) == 6);
	
	assert(binary_gcd(123,36) == 3);
	
	assert(binary_gcd(198,168) == 6);
	
	assert(binary_gcd(3915,825) == 15);
	
	assert(binary_gcd(34,19) == 1);
	
	assert(binary_gcd(177741,149553) == 783);
	
	assert(binary_gcd(816,2260) == 4);

	return 0;
}
