#include <cstdint>
#include <cassert>
#include <climits>
#include <iostream>
#include <stdexcept>

/*
 * The following code base was derived from StackOverflow at:
 *
 * https://stackoverflow.com/questions/14997165/fastest-way-to-get-a-positive-modulo-in-c-c
 *
 * for the function signature:
 * 
 * int modulo_Euclidean2(int a, int b)
 *
 * It was modified to comply with the problem specs.
 */

int64_t modulus(int64_t a,int64_t n)
{
	if ( n <= 0 ) return -1;

	int64_t m = a % n;

	return ( m < 0 ) ? m + n : m;
}

int main(int argc, char ** argv)
{
	// Now testing StackOverflow Program	
	
	assert(modulus(3,0) == -1); // Error: Cannot divide by 0. Error code -3 returned.
	
	assert(modulus(3,-1) == -1); // Error: Not allowed to make the base modulus a negative number. Error code -3 returned.
	
	assert(modulus(0,5) == 0);

	assert(modulus(230,17) == 9);

	assert(modulus(3,11) == 3);

	assert(modulus(-1,11) == 10);

	assert(modulus(17,5) == 2);

	assert(modulus(7,5) == 2);

	assert(modulus(3,7) == 3);

	assert(modulus(4,9) == 4);

	assert(modulus(-8,3) == 1);

	assert(modulus(-12,7) == 2);

	assert(modulus(90000000000,100000000000) == 90000000000);

	assert(modulus(-90000000000,100000000000) == 10000000000);

	assert(modulus(59049,13) == 3);

	assert(modulus(-59049,13) == 10);

	assert(modulus(LLONG_MIN,LLONG_MAX) == 9223372036854775806);

	assert(modulus(-5,5) == 0);

	assert(modulus(LLONG_MIN,1) == 0);

	assert(modulus(LLONG_MAX,LLONG_MAX) == 0);

	int64_t n = UINT_MAX;

	n++;

	assert(modulus(LLONG_MAX,n) == 4294967295);
	
	return 0;
}

