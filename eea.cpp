#include <cassert>
#include <climits>
#include <cstdint>
#include <iostream>
#include <vector>
using std::cout;
using std::endl;
using std::vector;

// Algorithm 2.104 Page 66 from Handbook of Applied Cryptography

// It is Integer Safe to use the '%' operator since it gives
//
// the correct result when a >= 0 and b > 0

// It accepts as input two positive integers a and b where a >= b

// In the below implementation where a < b the values of a and b are switched.

uint64_t gcd(uint64_t a,uint64_t b)
{
	uint64_t temp = 0;

	if ( a < b )
	{
		temp = a;

		a = b;

		b = temp;

	}

	uint64_t r = 0;

	while ( b != 0 )
	{
		r = a % b;

		a = b;

		b = r;	
	}

	return a;
}

// a and b are positive integers where a >= b
//
// If the function call is such that a < b the values are switched.

vector<uint64_t> eea(uint64_t a,uint64_t b)
{
	vector<uint64_t> result(3,0);

	uint64_t temp = 0;

	if ( a < b )
	{
		temp = a;

		a = b;

		b = temp;

	}

	uint64_t d = 0, x = 0, y = 0;

	if ( b == 0 )
	{
		d = a; x = 1; y = 0;	
	}
	
	uint64_t x_2 = 1, x_1 = 0, y_2 = 0, y_1 = 1, q = 0, r = 0;

	while ( b > 0 )
	{
		q = a / b; q = a / b ; r = a % b; x = x_2 - q * x_1 ; y = y_2 - q * y_1;

		a = b ; b = r ; x_2 = x_1 ; x_1 = x ; y_2 = y_1 ; y_1 = y;
	}

	result[0] = a;

	result[1] = x_2;

	result[2] = y_2;

	return result;

}
int main(void)
{
	vector<uint64_t> x = eea(240,46);

	assert(eea(ULLONG_MAX,ULLONG_MAX)[0] == ULLONG_MAX);
	
	assert(eea(ULLONG_MAX,1)[0] == 1);

	assert(eea(4864,3458)[0] == 38);
	
	assert(eea(1071,462)[0] == 21);
	
	assert(eea(270,192)[0] == 6);
	
	assert(eea(1512,444)[0] == 12);
	
	assert(eea(35,10)[0] == 5);
	
	assert(eea(112,42)[0] == 14);
	
	assert(eea(15,10)[0] == 5);
	
	assert(eea(35,10)[0] == 5);
	
	assert(eea(31,2)[0] == 1);
	
	assert(eea(30,20)[0] == 10);
	
	assert(eea(35,15)[0] == 5);
	
	assert(eea(33,27)[0] == 3);
	
	assert(eea(93,42)[0] == 3);
	
	assert(eea(36,48)[0] == 12);
	
	assert(eea(724,21)[0] == 1);
	
	assert(eea(97,60)[0] == 1);
	
	assert(eea(26,5)[0] == 1);
	
	assert(eea(12,30)[0] == 6);
	
	assert(eea(123,36)[0] == 3);
	
	assert(eea(198,168)[0] == 6);
	
	assert(eea(3915,825)[0] == 15);
	
	assert(eea(34,19)[0] == 1);
	
	assert(eea(177741,149553)[0] == 783);
	
	assert(eea(816,2260)[0] == 4);

	return 0;
}
