#include <cstdint>
#include <cassert>
#include <climits>
#include <iostream>
#include <stdexcept>

using std::cout;
using std::endl;
using std::stoll;
using std::to_string;

int64_t modulus_legacy(int64_t a, int64_t n)
{
	int64_t op1 = 0;

	if (n <= 0) return -3;

	int64_t floordiv = a / n;

	if ( a < 0 )
	{
		// The below assert and floordiv-- only applies when "a" is **not** an integral multiple of "n"
		
		if ( a % n != 0 )
		{	
			if ( floordiv - 1 >= 0 )
			{
				cout << "Floor Division underflowed to a positive result for " + to_string(a) + ", " + to_string(n) << endl;
				return -1;
			}

			floordiv--;
		}

		int64_t product = floordiv * n;
		
		if (product / n != floordiv)
		{
			cout << "Multiplication of floordiv to n resulted in underflow for " + to_string(a) + ", " + to_string(n) << endl;
			return -1;
		}

		product = -1 * product;
		
		if (product / -1 != -1 * product)
		{
			cout << "Multiplication by -1 to negative product resulted in overflow for " + to_string(a) + ", " + to_string(n) << endl;
			return -2;
		}

		op1 =  a + product; 
	}

	else if ( a == 0 )
	{
		op1 = a;
	}

	else // a > 0
	{
		int64_t product = floordiv * n;
		
		if ( product / n != floordiv)
		{
			cout << "Multiplication of floordiv to n resulted in overflow for " + to_string(a) + ",  " + to_string(n) << endl;
			return -2;
		
		}

		op1 = a - product;
	}	

	return op1 % n;	
}


int main(int argc, char ** argv)
{
	int64_t a_l = 0;
		
	int64_t n_l = 0;
	
	assert(modulus_legacy(3,0) == -3); // Error: Cannot divide by 0. Error code -3 returned.
	
	assert(modulus_legacy(3,-1) == -3); // Error: Not allowed to make the base modulus a negative number. Error code -3 returned.
				
	assert(modulus_legacy(0,5) == 0);

	assert(modulus_legacy(230,17) == 9);

	assert(modulus_legacy(3,11) == 3);

	assert(modulus_legacy(-1,11) == 10);

	assert(modulus_legacy(17,5) == 2);

	assert(modulus_legacy(7,5) == 2);

	assert(modulus_legacy(3,7) == 3);

	assert(modulus_legacy(4,9) == 4);

	assert(modulus_legacy(-8,3) == 1);

	assert(modulus_legacy(-12,7) == 2);

	assert(modulus_legacy(90000000000,100000000000) == 90000000000);

	assert(modulus_legacy(-90000000000,100000000000) == 10000000000);

	assert(modulus_legacy(59049,13) == 3);

	assert(modulus_legacy(-59049,13) == 10);

	assert(modulus_legacy(LLONG_MIN,LLONG_MAX) == -1); // Causes Overflow

	assert(modulus_legacy(-5,5) == 0);

	assert(modulus_legacy(LLONG_MIN,1) == 0);

	assert(modulus_legacy(LLONG_MAX,LLONG_MAX) == 0);

	n_l = UINT_MAX;

	n_l++;

	assert(modulus_legacy(LLONG_MAX,n_l) == 4294967295);
	
	return 0;
}
