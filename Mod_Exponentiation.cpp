#include <cstdint>
#include <cassert>
#include <cerrno>
#include <climits>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <stdexcept>
#include <string>
#include <vector>
using std::cout;
using std::endl;
using std::string;
using std::to_string;
using std::vector;

class Mod
{
	private:

		int64_t a;

		int64_t n;


	public:
		Mod(int64_t a,int64_t n)
		{
			if ( n <= 0 )
			{
				errno = -1;
				
				perror("Modulus base number n must not be less than or equal to 0");
				
				exit(-1);

			}
			
			this->n = n;

			this->a = modulus(a,n);


		}

		int64_t modulus(int64_t a,int64_t n)
		{
			int64_t m = a % n;

			return ( m < 0 ) ? m + n : m;
		}
		
		int64_t geta(void)
		{
			return this->a;
		}

		int64_t getn(void)
		{
			return this->n;
		}

		// Lowest possible value for x in operator+(int64_t b)
		
		// is -(2^63 - 2) = -2^63 + 2 although LLONG_MIN = -2^63
		//
		// since the Mod(int64_t a,int64_t n) constructor applies the input "a"
		//
		// to modulus(int64_t a).
		
		int64_t absolute_value(int64_t x)
		{
			return x < 0 ? -x : x;
		}
	
		// both a and b in (a + b) \mod n are positive

		Mod operator+(int64_t b)
		{
			int64_t a_neg = this->a - n; 

			int64_t b_pos = modulus(b,this->n);
			
			int64_t b_neg = b_pos - n;

			int64_t a_add = this->a < absolute_value(a_neg) ? this->a : a_neg;
			
			int64_t b_add = b_pos < absolute_value(b_neg) ? b_pos : b_neg;

			return Mod( a_add + b_add , this->n ); 	
		}
		
		Mod operator+(Mod b)
		{
			assert(b.getn() == this->n);

			int64_t a_neg = this->a - n; 

			int64_t b_pos = modulus(b.geta(),this->n);
			
			int64_t b_neg = b_pos - n;

			int64_t a_add = this->a < absolute_value(a_neg) ? this->a : a_neg;
			
			int64_t b_add = b_pos < absolute_value(b_neg) ? b_pos : b_neg;

			return Mod( a_add + b_add , this->n ); 	
		}
		
		Mod operator-(int64_t b)
		{
			int64_t a_neg = this->a - n; 

			int64_t b_pos = modulus(b,this->n);
			
			int64_t b_neg = b_pos - n;

			int64_t a_add = this->a < absolute_value(a_neg) ? this->a : a_neg;
			
			int64_t b_add = b_pos < absolute_value(b_neg) ? b_pos : b_neg;

			return Mod( a_add - b_add, this->n ); 	
		}
		
		Mod operator*(Mod b)
		{
			int64_t a_neg = this->a - n; 

			int64_t b_pos = modulus(b.geta(),this->n);
			
			int64_t b_neg = b_pos - n;

			int64_t a_mul = this->a < absolute_value(a_neg) ? this->a : a_neg;
			
			int64_t b_mul = b_pos < absolute_value(b_neg) ? b_pos : b_neg;
		
			int64_t throwaway = 0;
			
			if ( __builtin_mul_overflow(a_mul,b_mul,(int64_t*) &throwaway) == false )
			{
				return Mod(a_mul * b_mul,n);
			}
			
			return

				Mod(a_mul * (LLONG_MAX / a_mul),n) * ( b_mul / ( LLONG_MAX / a_mul ) )

				+

				Mod(a_mul,n) * ( b_mul - ( LLONG_MAX / a_mul ) * ( b_mul / ( LLONG_MAX / a_mul ) ) )

				;
 


		}
		
		Mod operator*(int64_t b)
		{
			int64_t a_neg = this->a - n; 

			int64_t b_pos = modulus(b,this->n);
			
			int64_t b_neg = b_pos - n;

			int64_t a_mul = this->a < absolute_value(a_neg) ? this->a : a_neg;
			
			int64_t b_mul = b_pos < absolute_value(b_neg) ? b_pos : b_neg;
		
			int64_t throwaway = 0;
			
			if ( __builtin_mul_overflow(a_mul,b_mul,(int64_t*) &throwaway) == false )
			{
				return Mod(a_mul * b_mul,n);
			}
			
			return

				Mod(a_mul * (LLONG_MAX / a_mul),n) * ( b_mul / ( LLONG_MAX / a_mul ) )

				+

				Mod(a_mul,n) * ( b_mul - ( LLONG_MAX / a_mul ) * ( b_mul / ( LLONG_MAX / a_mul ) ) )

				;
 

		}

		Mod exp(int64_t b)
		{
			if ( b < 0 ) { errno = -1; perror("Error: x for reverse_pos_x() must be positive"); exit(errno); }

			Mod result = Mod(1,this->n);

			Mod base = Mod(a,this->n);

			while ( b > 0 )
			{
				if ( b % 2 == 1 )
				{
					result = result * base;
				}

				b /= 2;

				base = base * base;
			}
				
			return result;
		}

		string toString(void)
		{
			return to_string(this->a) + " mod " + to_string(this->n);
		}

};

int main(void)
{
	uint64_t a = (uint64_t)pow(2,63), b = 0, n = 0;

	a--;

	Mod test = Mod(a,a);

	Mod sol = test.exp(a);

	assert(sol.geta() == 0);

	a = (uint64_t)pow(2,62);

	n = (uint64_t)pow(2,63); n--;

	test = Mod(a,n);

	sol = test.exp(a);

	assert(sol.geta() == 576460752303423488);

	test = Mod(a,n);

	sol = test.exp(2);

	assert(sol.geta() == 2305843009213693952);

	test = Mod(3,3);

	sol = test.exp(5);

	assert(sol.geta() == 0);

	test = Mod(1024,1023);

	sol = test.exp(1024);

	assert(sol.geta() == 1);

	a = (uint64_t)pow(2,60);

	b = (uint64_t)pow(2,19);

	n = (uint64_t)pow(2,63); n--;

	test = Mod(a,n);

	sol = test.exp(b);

	assert(sol.geta() == 144115188075855872);

	test = Mod(2,n);

	sol = test.exp(27);

	assert(sol.geta() == 134217728);
	
	n = (uint64_t)pow(2,63); n--;

	test = Mod(102325032992938,n);

	sol = test.exp(n);

	assert(sol.geta() == 8422387123304720545);

	test = Mod(5,101);

	sol = test.exp(69);

	assert(sol.geta() == 37);

	test = Mod(50,13);

	sol = test.exp(50);

	assert(sol.geta() == 4);

	test = Mod(6,23);

	sol = test.exp(1000);

	assert(sol.geta() == 4);

	a = (uint64_t)pow(2,63); a -= 3;

	n = (uint64_t)pow(2,63); n--;

	test = Mod(a,n);

	sol = test.exp(3);

	assert(sol.geta() == 9223372036854775799);

	a = (uint64_t)pow(2,62); a--;

	n = (uint64_t)pow(2,62);
	
	test = Mod(a,n);

	sol = test.exp(a);

	assert(sol.geta() == 4611686018427387903);

	a = (uint64_t)pow(2,32); a--;

	n = (uint64_t)pow(2,61); n--;

	test = Mod(a,n);

	sol = test.exp((uint64_t)pow(2,32));

	assert(sol.geta() == 1698073614482834710);

	a = (uint64_t)pow(2,30);
	
	n = (uint64_t)pow(2,61); n--;

	test = Mod(a,n);

	sol = test.exp(a);

	assert(sol.geta() == 2147483648);
	
	a = (uint64_t)pow(2,62);
	
	n = (uint64_t)pow(2,63); n--;

	sol = test.exp(a);

	assert(sol.geta() == 576460752303423488);

	return 0;
}
