#include <cassert>
#include <climits>
#include <cstdint>
#include <iostream>
#include <vector>
using std::cout;
using std::endl;
using std::vector;

// Algorithm 2.104 Page 66 from Handbook of Applied Cryptography

// It is Integer Safe to use the '%' operator since it gives
//
// the correct result when a >= 0 and b > 0

// It accepts as input two positive integers a and b where a >= b

// In the below implementation where a < b the values of a and b are switched.

int64_t gcd(int64_t a,int64_t b)
{
	assert(a > 0);
	
	assert(b > 0);

	int64_t temp = 0;

	if ( a < b )
	{
		temp = a;

		a = b;

		b = temp;

	}

	int64_t r = 0;

	while ( b != 0 )
	{
		r = a % b;

		a = b;

		b = r;	
	}

	return a;
}

// a and b are positive integers where a >= b
//
// If the function call is such that a < b the values are switched.

vector<int64_t> bin_eea(int64_t x,int64_t y)
{
	vector<int64_t> result(3,0);

	assert( x > 0);

	assert( y > 0);

	int64_t temp = 0;

	if ( x < y )
	{
		temp = x;

		x = y;

		y = temp;

	}

	int64_t g = 1;

	while ( x % 2 == 0 && y % 2 == 0 ) { x >>= 1; y >>= 1; g <<= 1;}

	int64_t u = x, v = y, A = 1, B = 0, C = 0, D = 1;
		
	step_four:

	while ( u % 2 == 0 )
	{
		u >>= 1;

		if ( (A % 2 == 0) && (B % 2 == 0) )
		{
			A >>= 1;

			B >>= 1;
		}

		else { A = (A + y) / 2 ; B = (B - x) / 2; }

	}

		while ( v % 2 == 0 )
		{
			v >>= 1;

			if ( C % 2 == 0 && D % 2 == 0 )
			{
				C >>= 1;

				D >>= 1;

				C = (C + y) / 2;

				D = (D - x) / 2;
			}
		}


		if ( u >= v ) { u = u - v; A = A - C ; B = B - D; }

		else { v = v - u; C = C - A ; D = D - B; }
		
		if ( u != 0 ) { goto step_four; }
		
	return {C , D , g * v };

}

int64_t mod_inverse(int64_t a, int64_t m)
{
	if ( gcd(a,m) != 1 ) return -1;

	return bin_eea(m,a)[1];
}

int main(void)
{
	vector<int64_t> x;

	assert(bin_eea(LLONG_MAX,LLONG_MAX)[2] == LLONG_MAX);
	
	assert(bin_eea(LLONG_MAX,1)[2] == 1);

	assert(bin_eea(4864,3458)[2] == 38);
	
	assert(bin_eea(1071,462)[2] == 21);
	
	assert(bin_eea(270,192)[2] == 6);
	
	assert(bin_eea(1512,444)[2] == 12);
	
	assert(bin_eea(35,10)[2] == 5);
	
	assert(bin_eea(112,42)[2] == 14);
	
	assert(bin_eea(15,10)[2] == 5);
	
	assert(bin_eea(35,10)[2] == 5);
	
	assert(bin_eea(31,2)[2] == 1);
	
	assert(bin_eea(30,20)[2] == 10);
	
	assert(bin_eea(35,15)[2] == 5);
	
	assert(bin_eea(33,27)[2] == 3);
	
	assert(bin_eea(93,42)[2] == 3);
	
	assert(bin_eea(36,48)[2] == 12);
	
	assert(bin_eea(724,21)[2] == 1);
	
	assert(bin_eea(97,60)[2] == 1);
	
	assert(bin_eea(26,5)[2] == 1);
	
	assert(bin_eea(12,30)[2] == 6);
	
	assert(bin_eea(123,36)[2] == 3);
	
	assert(bin_eea(198,168)[2] == 6);
	
	assert(bin_eea(3915,825)[2] == 15);
	
	assert(bin_eea(34,19)[2] == 1);
	
	assert(bin_eea(177741,149553)[2] == 783);
	
	assert(bin_eea(816,2260)[2] == 4);

	return 0;
}
