#include <cstdint>
#include <cassert>
#include <cerrno>
#include <climits>
#include <cmath>
#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <stdexcept>
#include <string>
#include <sys/random.h>
#include <vector>
using std::cout;
using std::endl;
using std::string;
using std::to_string;
using std::vector;

class Mod
{
	private:
		bool is_moduli_prime = 0;
		
		bool checked_is_moduli_prime = 0;

		int64_t a = 0;

		int64_t n = 1;


	public:
		
		Mod(int64_t a,int64_t n)
		{
			if ( n <= 0 )
			{
				errno = -1;
				
				perror("Modulus base number n must not be less than or equal to 0");
				
				exit(-1);

			}
			
			this->n = n;

			this->a = modulus(a,n);


		}

		int64_t modulus(int64_t a,int64_t n)
		{
			int64_t m = a % n;

			return ( m < 0 ) ? m + n : m;
		}
		
		int64_t geta(void)
		{
			return this->a;
		}

		int64_t getn(void)
		{
			return this->n;
		}

		// Lowest possible value for x in operator+(int64_t b)
		
		// is -(2^63 - 2) = -2^63 + 2 although LLONG_MIN = -2^63
		//
		// since the Mod(int64_t a,int64_t n) constructor applies the input "a"
		//
		// to modulus(int64_t a).
		
		int64_t absolute_value(int64_t x)
		{
			return x < 0 ? -x : x;
		}
	
		// both a and b in (a + b) \mod n are positive

		Mod operator+(int64_t b)
		{
			int64_t a_neg = this->a - n; 

			int64_t b_pos = modulus(b,this->n);
			
			int64_t b_neg = b_pos - n;

			int64_t a_add = this->a < absolute_value(a_neg) ? this->a : a_neg;
			
			int64_t b_add = b_pos < absolute_value(b_neg) ? b_pos : b_neg;

			return Mod( a_add + b_add , this->n ); 	
		}
		
		Mod operator+(Mod b)
		{
			assert(b.getn() == this->n);

			int64_t a_neg = this->a - n; 

			int64_t b_pos = modulus(b.geta(),this->n);
			
			int64_t b_neg = b_pos - n;

			int64_t a_add = this->a < absolute_value(a_neg) ? this->a : a_neg;
			
			int64_t b_add = b_pos < absolute_value(b_neg) ? b_pos : b_neg;

			return Mod( a_add + b_add , this->n ); 	
		}
		
		Mod operator-(int64_t b)
		{
			int64_t a_neg = this->a - n; 

			int64_t b_pos = modulus(b,this->n);
			
			int64_t b_neg = b_pos - n;

			int64_t a_add = this->a < absolute_value(a_neg) ? this->a : a_neg;
			
			int64_t b_add = b_pos < absolute_value(b_neg) ? b_pos : b_neg;

			return Mod( a_add - b_add, this->n ); 	
		}
		
		Mod operator*(Mod b)
		{
			int64_t a_neg = this->a - n; 

			int64_t b_pos = modulus(b.geta(),this->n);
			
			int64_t b_neg = b_pos - n;

			int64_t a_mul = this->a < absolute_value(a_neg) ? this->a : a_neg;
			
			int64_t b_mul = b_pos < absolute_value(b_neg) ? b_pos : b_neg;
		
			int64_t throwaway = 0;
			
			if ( __builtin_mul_overflow(a_mul,b_mul,(int64_t*) &throwaway) == false )
			{
				return Mod(a_mul * b_mul,n);
			}
			
			return

				Mod(a_mul * (LLONG_MAX / a_mul),n) * ( b_mul / ( LLONG_MAX / a_mul ) )

				+

				Mod(a_mul,n) * ( b_mul - ( LLONG_MAX / a_mul ) * ( b_mul / ( LLONG_MAX / a_mul ) ) )

				;
 


		}
		
		Mod operator*(int64_t b)
		{
			int64_t a_neg = this->a - n; 

			int64_t b_pos = modulus(b,this->n);
			
			int64_t b_neg = b_pos - n;

			int64_t a_mul = this->a < absolute_value(a_neg) ? this->a : a_neg;
			
			int64_t b_mul = b_pos < absolute_value(b_neg) ? b_pos : b_neg;
		
			int64_t throwaway = 0;
			
			if ( __builtin_mul_overflow(a_mul,b_mul,(int64_t*) &throwaway) == false )
			{
				return Mod(a_mul * b_mul,n);
			}
			
			return

				Mod(a_mul * (LLONG_MAX / a_mul),n) * ( b_mul / ( LLONG_MAX / a_mul ) )

				+

				Mod(a_mul,n) * ( b_mul - ( LLONG_MAX / a_mul ) * ( b_mul / ( LLONG_MAX / a_mul ) ) )

				;
 

		}

		Mod exp(int64_t b)
		{
			if ( b < 0 ) { errno = -1; perror("Error: x for reverse_pos_x() must be positive"); exit(errno); }

			Mod result = Mod(1,this->n);

			Mod base = Mod(a,this->n);

			while ( b > 0 )
			{
				if ( b % 2 == 1 )
				{
					result = result * base;
				}

				b /= 2;

				base = base * base;
			}

			return result;
		}
		
		int64_t gcd(int64_t a,int64_t b)
		{
			assert(a > 0);
			
			assert(b > 0);

			int64_t temp = 0;

			if ( a < b )
			{
				temp = a;

				a = b;

				b = temp;

			}

			int64_t r = 0;

			while ( b != 0 )
			{
				r = a % b;

				a = b;

				b = r;	
			}

			return a;
		}


		int64_t eea(int64_t a,int64_t b,int64_t &x, int64_t &y)
		{
			if ( b == 0 )
			{
				x = 1;

				y = 0;

				return a;
			}

			int64_t x_1 = 0, y_1 = 0;

			int64_t d = eea(b,modulus(a,b),x_1,y_1);

			x = y_1;

			y = x_1 - y_1 * ( a / b );

			return d;
		}

		// Calculate the first 128 odd primes using Trial Division

		vector<int64_t> first_128_odd_primes(void)
		{
			vector<int64_t> primes;

			int64_t num = 3;

			primes.push_back(num);

			num += 2;
				
			size_t i = 0;

			while ( primes.size() < 128 )
			{

				for ( i = 0 ; i < primes.size() ; i++ )
				{
					if ( num % primes[i] == 0 ) break;
				}

				if ( i == primes.size() ) primes.push_back(num);

				num += 2;
			}

			return primes;
		}
		
		// Below taken from the paper "A Performant, Misuse-Resistant API for Primality Testing": Section 2
		
		bool miller_rabin(void)
		{
			if ( this->n <= 0 ) { errno = -1; perror("n in miller_rabin() must be positive") ; exit(errno); }
			
			if ( this->n == 1 ) return false;

			if ( this->n % 2 == 0 ) return false;

			const int64_t f = static_cast<int64_t>(log(n-1.0)/log(2.0));

		//	cout << "f: " << f << endl;
			
			int64_t e = 0;

			int64_t d = ( this->n - 1)/(static_cast<int64_t>(pow(2,e)));
			
		//	cout << "d: " << d << endl;

			while	(
					(

						( d % 2 != 0 )
						
						||
						
						( (this->n - 1) % ( static_cast<int64_t>( pow(2,e) ) ) != 0 )

					)

					&&

					( e <= f )
				
				)
			{
				e++;	

		//		cout << "e: " << e << endl;

				d = ( this->n - 1)/(static_cast<int64_t>(pow(2,e)));
			
		//		cout << "d: " << d << endl;
			}

		//	cout << "Made it" << endl;

			// Choose a random value for a from a CSPRNG

			int64_t mra = 0;

			while ( mra <= 0 )
			{
				getrandom(&mra,sizeof(int64_t),GRND_NONBLOCK);

				if ( errno == EAGAIN )
				{
					perror("getrandom() in miller-rabin blocked. Abort!");

					exit(errno);
				}

		//		cout << "mra: " << mra << endl;
			}
			
			Mod base = Mod(mra,this->n);

			Mod raise = base.exp(d);

			if ( raise.geta() ==  1 ) return true;

			int64_t i = 0;

			int64_t power  = 0;

			while ( i < e )
			{
				power = static_cast<int64_t>(pow(2,i)) * d;

				base = Mod(mra,this->n);

				raise = base.exp(power);

				if ( raise.geta() == modulus(-1,this->n) ) return true;

				i++;
			}

			return false;	
				
		}

		// Perform 64 rounds of Miller-Rabin using 64 distinct, random values for a
		//
		// The values for a must be obtained from a Cryptographically Secure Pseudo Random Number Generator
		//
		// MR64() has a chance of error that is less than or equal to 2^(-128).

		bool MR64(void)
		{
			for ( size_t i = 0; i < 64 ; i++ )
			{
		//		cout << "Round " << i << endl;

				if ( miller_rabin() == false ) { this->is_moduli_prime = false ; return false; }
			}
			
			this->is_moduli_prime = true;

			return true;
				
		}

		bool is_modulus_prime()
		{
			if ( this->checked_is_moduli_prime == 1 ) return is_moduli_prime;

			if ( this->n == 2 )
			{
				this->checked_is_moduli_prime = 1;

				this->is_moduli_prime = 1;

				return true;
		
			}

			if ( this->n % 2 == 0 )
			{
				this->checked_is_moduli_prime = 0;

				this->is_moduli_prime = 0;

				return false;
			}

			vector<int64_t> first_odd_128_primes = first_128_odd_primes();

			for ( size_t i = 0 ; i < first_odd_128_primes.size() ; i++ )
			{
				if ( this->n % first_odd_128_primes[i] == 0 )
				{

					this->checked_is_moduli_prime = 0;

					this->is_moduli_prime = 0;

					return false;
				}
			}

			if ( MR64() == true )
			{
				this->checked_is_moduli_prime = 1;

				this->is_moduli_prime = 1;

				return true;
			}
				
			this->checked_is_moduli_prime = 0;

			this->is_moduli_prime = 0;

			return false;
		}

		Mod inverse(void)
		{
			if ( this->a > this->n ) { errno = -2; perror("a > n in mod_inverse"); exit(errno); }

			int64_t x = 0, y = 0;
			
			int64_t g = eea(this->a,this->n,x,y);

			if ( g != 1 ) { errno = -1 ; perror("Mod object does not have an inverse"); exit(errno); }

			else
			{
				x = modulus(x,this->n);
			}

			return Mod(x,this->n);
		}

		int64_t inverse_moduli_prime_int64_t(int64_t a, int64_t m)
		{

			if ( m < 2 )
			{ 
				errno = -1; perror("m in mod_inverse() is not a prime number as expected"); exit(errno);
			}

			if ( a < 0 )
			{
				errno = -2; perror("a in mod_inverse() must be positive"); exit(errno);
			}

			if ( a >= m )
			{
				errno = -3; perror("a >= m in mod_inverse(). 0 < a < m."); exit(errno);
			}

			if ( gcd(a,m) != 1 ) return -1;
			
			return a <= 1 ? a : m - ( m / a ) * inverse_moduli_prime_int64_t( m % a,m) % m;
		
		}

		Mod inverse_moduli_prime(void)
		{
			if ( this->checked_is_moduli_prime == false )
			{
				is_modulus_prime();
			}

			if ( this->checked_is_moduli_prime == true && this->is_moduli_prime == false )
			{
				errno = -1;

				perror("Error: modulus number n in inverse_moduli_prime is composite");

				exit(errno);
			}
			
			return Mod(inverse_moduli_prime_int64_t(this->a,this->n),this->n);
		}


		

		string toString(void)
		{
			return to_string(this->a) + " mod " + to_string(this->n);
		}

};

int main(void)
{
	// The following prime numbers are documented at: https://en.wikipedia.org/wiki/List_of_prime_numbers

	Mod test = Mod(5,3449);

	assert(test.is_modulus_prime() == true);

	uint64_t moduli = (uint64_t)pow(2,61); moduli--;

	test = Mod(5,moduli);
	
	assert(test.is_modulus_prime() == true);

	test = Mod(5,7919);
	
	assert(test.is_modulus_prime() == true);

	test = Mod(5,27644437);
	
	assert(test.is_modulus_prime() == true);

	test = Mod(5,27); // a known composite
	
	assert(test.is_modulus_prime() == false);

	test = Mod(5,87178291199);
	
	assert(test.is_modulus_prime() == true);

	test = Mod(5,3331113965338635107);
	
	assert(test.is_modulus_prime() == true);

	test = Mod(5,3331113965338635105); // a known composite
	
	assert(test.is_modulus_prime() == false);

	test = Mod(5,22815088913);
	
	assert(test.is_modulus_prime() == true);

	test = Mod(5,3318308475676071413);
	
	assert(test.is_modulus_prime() == true);

	test = Mod(5,22815088913);
	
	assert(test.is_modulus_prime() == true);

	test = Mod(5,8589935681);
	
	assert(test.is_modulus_prime() == true);

	test = Mod(5,8589935679); // a known composite
	
	assert(test.is_modulus_prime() == false);

	test = Mod(5,59604644783353249); 

	assert(test.is_modulus_prime() == true);

	test = Mod(5,32361122672259149); 

	assert(test.is_modulus_prime() == true);

	test = Mod(5,32361122672259147); // a known composite 

	assert(test.is_modulus_prime() == false);

	test = Mod(5,768614336404564651);

	assert(test.is_modulus_prime() == true);

	test = Mod(5,188748146801);

	assert(test.is_modulus_prime() == true);

	test = Mod(5,188748146803);

	assert(test.is_modulus_prime() == true);
	
	test = Mod(5,188748146805); // a known composite

	assert(test.is_modulus_prime() == false);
	
	test = Mod(5,32212254719);

	assert(test.is_modulus_prime() == true);
	
	test = Mod(5,32212254721); // a known composite

	assert(test.is_modulus_prime() == false);
	
	test = Mod(5,9809862296159); 

	assert(test.is_modulus_prime() == true);
	
	test = Mod(5,9809862296161); // a known composite 

	assert(test.is_modulus_prime() == false);
	
	test = Mod(5,2549536629329); 

	assert(test.is_modulus_prime() == true);
	
	test = Mod(5,2549536629331); // a known composite 

	assert(test.is_modulus_prime() == false);
	
	test = Mod(5,1111111111111111111);

	assert(test.is_modulus_prime() == true);
	
	test = Mod(5,1111111111111111113); // a known composite

	assert(test.is_modulus_prime() == false);
	
	test = Mod(5,492366587);

	assert(test.is_modulus_prime() == true);
	
	test = Mod(5,492366589); // a known composite

	assert(test.is_modulus_prime() == false);
	
	test = Mod(5,59969537);

	assert(test.is_modulus_prime() == true);
	
	test = Mod(5,59969539); // a known composite

	assert(test.is_modulus_prime() == false);
	
	test = Mod(5,108086391056891903);

	assert(test.is_modulus_prime() == true);
	
	test = Mod(5,108086391056891905); // a known composite

	assert(test.is_modulus_prime() == false);
	
	test = Mod(5,206158430209);

	assert(test.is_modulus_prime() == true);
	
	test = Mod(5,206158430211); // a known composite

	assert(test.is_modulus_prime() == false);
	
	test = Mod(5,6597069766657);

	assert(test.is_modulus_prime() == true);
	
	test = Mod(5,6597069766659); // a known composite

	assert(test.is_modulus_prime() == false);
	
	test = Mod(5,715827883);

	assert(test.is_modulus_prime() == true);
	
	test = Mod(5,715827885); // a known composite

	assert(test.is_modulus_prime() == false);
	
	test = Mod(5,15285151248481);

	assert(test.is_modulus_prime() == true);
	
	test = Mod(5,15285151248483); // a known composite

	assert(test.is_modulus_prime() == false);
	
	test = Mod(5,44560482149);

	assert(test.is_modulus_prime() == true);
	
	test = Mod(5,44560482151);

	assert(test.is_modulus_prime() == true);
	
	return 0;
}
