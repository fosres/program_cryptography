#include <cstdint>
#include <cassert>
#include <climits>
#include <iostream>
#include <stdexcept>

/*
 * The following code base was derived from StackOverflow at:
 *
 * https://stackoverflow.com/questions/14997165/fastest-way-to-get-a-positive-modulo-in-c-c
 *
 * for the function signature:
 * 
 * int modulo_Euclidean2(int a, int b)
 *
 * It was modified to comply with the problem specs.
 */

int64_t mod(int64_t a,int64_t n)
{
	if ( n <= 0 ) return -1;

	return a - n * (a / n);
}

int main(int argc, char ** argv)
{
	// Now testing StackOverflow Program	
	
	
	assert(mod(3,-1) == -1 ); // Error: Not allowed to make the base mod a negative number. Error code -3 returned.
	
	assert(mod(0,5) == (0 % 5) );

	assert(mod(230,17) == (230 % 17) );

	assert(mod(3,11) == (3 % 11) );

	assert(mod(-1,11) == -1 % 11);

	assert(mod(17,5) == (17 % 5) );

	assert(mod(7,5) == (7 % 5) );

	assert(mod(3,7) == (3 % 7) );

	assert(mod(4,9) == (4 % 9) );

	assert(mod(-8,3) == (-8 % 3) );

	assert(mod(-12,7) == (-12 % 7) );

	assert(mod(90000000000,100000000000) == ( 90000000000 % 100000000000) );

	assert(mod(-90000000000,100000000000) == ( -90000000000 % 100000000000) );

	assert(mod(59049,13) == (59049 % 13) );

	assert(mod(-59049,13) == (-59049 % 13) );

	assert(mod(LLONG_MIN,LLONG_MAX) == (LLONG_MIN % LLONG_MAX) );

	assert(mod(-5,5) == (-5 % 5) );

	assert(mod(LLONG_MIN,1) == (LLONG_MIN % 1) );

	assert(mod(LLONG_MAX,LLONG_MAX) == (LLONG_MAX % LLONG_MAX) );

	int64_t n = UINT_MAX;

	n++;

	assert(mod(LLONG_MAX,n) == (LLONG_MAX % n) );
	
	assert(mod(0,1) == (0 % 1) );
	
	assert(mod(0,7) == (0 % 7) );
	
	assert(mod(0,LLONG_MAX) == (0 % LLONG_MAX) );
	
	assert(mod(3,4) == (3 % 4) );
	
	assert(mod(4,3) == (4 % 3) );
	
	assert(mod(-4,3) == (-4 % 3) );
	
	assert(mod(-6,3) == (-6 % 3) );
	
	assert(mod(-7,3) == (-7 % 3) );
	
	assert(mod(-7,3) == (-7 % 3) );

	assert(mod(-8,3) == (-8 % 3) );
	
	assert(mod(-9,3) == (-9 % 3) );
	
	assert(mod(LLONG_MIN,LLONG_MAX) == (LLONG_MIN % LLONG_MAX) );
	
	assert(mod(-50,43) == (-50 % 43) );
	
	assert(mod(-99,22) == (-99 % 22) );
	
	assert(mod(-46,13) == (-46 % 13) );
	
	assert(mod(-37,12) == (-37 % 12) );
	
	assert(mod(-37,12) == (-37 % 12) );
	
	return 0;
}

